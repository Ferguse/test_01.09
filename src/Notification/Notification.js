import React, { Component } from 'react';
import PropTypes from 'prop-types';

// styled
import Container from './styled/Container';
import OverflowElement from '../App/styled/OverflowElement';

// icons
import ok from '../assets/icons/ok.png';

class Notification extends Component {
  render() {
    const { notification } = this.props;
    return (
      <Container>
        {
          notification.map((note, i) => (
            <div key={i}>
              <OverflowElement>{note}</OverflowElement>
              <img src={ok} alt='ok icon'/>
              <span>добавлено в ваши увлечения</span>
            </div>
          ))
        }
      </Container>
    );
  }
}

Notification.propTypes = {
  notification: PropTypes.array.isRequired
};

export default Notification;
