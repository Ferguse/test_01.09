import React, { Component } from 'react';

// config
import hobby from '../config';

// styled
import Container from './styled/Container';

// components
import Section from '../Section';
import Notification from '../Notification';
import Input from '../Section/styled/Input';
import Form from '../Form';

class App extends Component {
  state = {
    myHobbies: hobby.myHobbies,
    friendHobbies: hobby.friendHobbies,
    inputValue: '',
    notification: [],
    warningForm: {
      isOpen: false,
      target: ''
    }
  };

  componentDidMount = () => {
    window.addEventListener('keyup', (e) => {
      if (e.keyCode === 13) {
        this.setState({
          myHobbies: [...this.state.myHobbies, this.state.inputValue],
          inputValue: ''
        });
      }
    })
  };

  handleChange = e => {
    const value = e.target.value;
    this.setState({ inputValue: value});
  };

  handleDelete = (target) => () =>
    this.setState(({ myHobbies }) =>
      ({ myHobbies: myHobbies.filter(i => i !== target)}));

  handleAdd = (target) => () =>
    this.setState(({ myHobbies, notification }) =>
      ({ myHobbies: [...myHobbies, target],
        notification: [...notification, target]
      }));

  handleOpenForm = (target) => () =>
    this.setState({ warningForm : { isOpen: true, target } });

  handleCloseForm = () =>
    this.setState({ warningForm: { isOpen: false, target: null } });

  render() {
    const { myHobbies, friendHobbies, notification, warningForm } = this.state;

    return (
      <Container>
        <Section
          title='О себе'
          hobbies={myHobbies}
          handleClick={this.handleDelete}
          isMyHobby>
          <Input
            type='text'
            placeholder='Введите текст'
            onChange={this.handleChange}
            value={this.state.inputValue}
          />
        </Section>
        <Section
          title='Интересы друга'
          hobbies={friendHobbies}
          handleClick={this.handleAdd}
          isMyHobby={false}
          handleOpenForm={this.handleOpenForm}
        />
        <Notification
          data={notification}
          notification={notification}
        />
        {
          warningForm.isOpen &&
          <Form target={warningForm.target} handleCloseForm={this.handleCloseForm} />
        }
      </Container>
    );
  }
}

export default App;
