import styled from 'styled-components';

export default styled.div`
  position: relative;
  width: 90%;
  margin: 0 auto;
  padding: 20px;
  font-family: Arial, sans-serif;
`;
