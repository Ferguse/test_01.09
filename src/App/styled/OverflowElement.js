import styled from 'styled-components';

export default styled.div`
  position: relative;
  white-space: nowrap;
  overflow: hidden;
  width: 100%;
  
  &::after {
    background: linear-gradient(to right, transparent, #fff);
    position: absolute;
    content: '';
    width: 20px;
    height: 100%;
    right: 0;
    top: 0;
  }
`;
