import styled from 'styled-components';

export default styled.ul`
  position: relative;
  list-style: none;
  padding: 0;
  margin: 0;
  
  li {
    font-size: 12px;
    margin: 10px 0;
  }
`
