import styled from 'styled-components';

export default styled.div`
  position: relative;
  margin: 20px 0;
  
  span {
    background-color: #fff;
    padding: 0 10px;
  }
  
  &::after {
    position: absolute;
    content: '';
    width: 100%;
    height: 2px;
    background-color: #eee;
    top: 50%;
    left: 0;
    z-index: -1;
  }
`
