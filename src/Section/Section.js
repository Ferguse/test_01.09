import React, { PureComponent }  from 'react';
import PropTypes from 'prop-types';

// styled
import Container from './styled/Container';
import Title from './styled/Title';
import List from './styled/List';
import Hobby from './styled/Hobby';

// components
import OpenButton from './OpenButton';
import Item from './Item';

class Section extends PureComponent {
  state = {
    isOpenHobbies: false
  };

  handleShow = () => this.setState({ isOpenHobbies: !this.state.isOpenHobbies });

  render() {
    const { title, hobbies, children, handleClick, isMyHobby, handleOpenForm } = this.props;
    const { isOpenHobbies } = this.state;
    return (
      <Container>
        <Title>{title}</Title>
        <Hobby>
          <span>Хобби</span>
        </Hobby>
        { children }
        <List>
          {
            hobbies.map((hobby, i) =>  {
              if ( i < 3 ) {
                return  (
                  <Item
                    key={i}
                    text={hobby}
                    handleClick={handleClick}
                    isMyHobby={isMyHobby}
                    handleOpenForm={handleOpenForm}

                  />
                )
              }
              return null;
            })
          }
          {
            hobbies.map((hobby, i) =>  {
              if (i >= 3 && isOpenHobbies) {
                return (
                  <Item
                    key={i}
                    text={hobby}
                    handleClick={handleClick}
                    isMyHobby={isMyHobby}
                    handleOpenForm={handleOpenForm}
                  />
                )
              }
              return null;
            })
          }
        </List>
        {
          hobbies.length > 3 &&
          hobbies.length !== 0 &&
          <OpenButton
            handleShow={this.handleShow}
            isOpenHobbies={isOpenHobbies}
            count={hobbies.length - 3}
          />
        }
      </Container>
    )
  }
}

Section.propTypes = {
  title: PropTypes.string.isRequired,
  hobbies: PropTypes.array.isRequired,
  children: PropTypes.object,
  handleClick: PropTypes.func.isRequired,
  isMyHobby: PropTypes.bool.isRequired,
  handleOpenForm: PropTypes.func
};

Section.defaultValue = {
  children: null,
  handleOpenForm: null
};

export default Section;
