import React from 'react';
import PropTypes from 'prop-types';

// styled
import Container from './Container';

const variation = (count) => {
  if (count > 0 && count <= 4) return 'а';
  if (count > 4) return 'ов';
  return '';
};

const OpenButton = ({ count, handleShow, isOpenHobbies }) => (
  <Container onClick={handleShow}>
    {
      (!isOpenHobbies) ? `Еще ${count} интерес${variation(count)}` : 'Скрыть хобби'
    }
  </Container>
);

OpenButton.propTypes = {
  count: PropTypes.number.isRequired,
  handleShow: PropTypes.func.isRequired,
  isOpenHobbies: PropTypes.bool.isRequired
};

export default OpenButton;
