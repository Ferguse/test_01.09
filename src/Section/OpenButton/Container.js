import styled from 'styled-components';

export default styled.button`
  background-color: transparent;
  border: none;
  outline: none;
  color: orange;
  text-decoration: underline;
`;
