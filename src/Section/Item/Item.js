import React from 'react';
import PropTypes from 'prop-types';

// icons
import add from '../../assets/icons/add.gif';
import close from '../../assets/icons/close.png';
import warn from '../../assets/icons/warn.png';

// styled
import Container from './styled/Container';
import Warning from './styled/Warning';
import Content from './styled/Content';
import OverflowElement from '../../App/styled/OverflowElement';

const Item =  ({ text, handleClick, isMyHobby, handleOpenForm }) => (
  <Container>
    <Content>
      <img src={(isMyHobby) ? close : add } alt='icon' onClick={handleClick(text)} />
      <OverflowElement>{ text }</OverflowElement>
    </Content>
    {
      !isMyHobby &&
      <Warning onClick={handleOpenForm(text)}>
        <img src={warn} alt='warning' />
        <span>Пожаловаться</span>
      </Warning>
    }
  </Container>
);

Item.propTypes = {
  text: PropTypes.string.isRequired,
  handleClick: PropTypes.func.isRequired,
  isMyHobby: PropTypes.bool.isRequired,
  handleOpenForm: PropTypes.func
};

Item.defaultProps = {
  handleOpenForm: null
};

export default Item;
