import styled from 'styled-components';

export default styled.div`
  overflow: hidden;
  display: flex;
  width: 100%;
  
  img {
    transition: 0.5s;
    opacity: 0;
    margin: 0 10px;
  }
  
  &:hover {
    img {
      opacity: 1;
    }
  }
`;
