import styled from 'styled-components';

export default styled.li`
  font-size: 10px;
  display: flex;
  justify-content: space-between;
  
  
  &:hover > span {
    opacity: 1;
  }
  
  & > span {
    transition: 0.5s;
    opacity: 0;
  }
`;

