import styled from 'styled-components';

export default styled.span`
  position: relative;
  padding: 0 25px;
  display: flex;
`;
