import styled from 'styled-components';

export default styled.div`
  background-color: #fff;
  position: absolute;
  bottom: 20px;
  right: 20px;
  padding: 20px;
  box-shadow: 0 0 10px 0 rgba(0, 0, 0, 0.1);
  display: flex;
  flex-direction: column;
  max-width: 300px;
  min-height: 150px;
  
  span {
    margin: 10px 0;
    display: flex;
  }
  
  textarea {
    margin: 10px 0;
  }
  
`;
