import styled from 'styled-components';

export default styled.div`
  position: absolute;
  width: 20px;
  height: 20px;
  top: 10px;
  right: 10px;
  
  img {
    width: 100%;
  }
`;
