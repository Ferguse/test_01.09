import React from 'react';
import PropTypes from 'prop-types';

// icon
import close from '../assets/icons/close.png';

// styled
import Container from './styled/Container';
import OverflowElement from '../App/styled/OverflowElement';
import Close from './styled/Close';

const Form = ({ target, handleCloseForm }) => (
  <Container>
    <Close onClick={handleCloseForm}>
      <img src={close} alt='close' />
    </Close>
    <span>Причина для жалобы на хобби: </span>
    <OverflowElement>{ target }</OverflowElement>
    <textarea cols="30" rows="10" />
  </Container>
);

Form.propTypes = {
  target: PropTypes.string.isRequired,
  handleCloseForm: PropTypes.func.isRequired
};

export default Form;

